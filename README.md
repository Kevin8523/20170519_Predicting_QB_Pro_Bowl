# 20170519_Predicting_QB_Pro_Bowl
A study on College Quarterbacks and how well they translate into the pros

## Overview 
- Personal project analyzing college quarterbacks and predicting wheter they will make the pro bowl within 5 years.
- Difficult finding data, obtained data from the follwing sites:

## Data Gathered
- College Football Stats
  - 1995 - 2017
- Coaches
- College Football Rating / Standings / Conference / ETC 
- Combine Stats
- Wonderlic Score
- Pro Bowl 

## Data Sources
- http://www.repole.com/sun4cast/data.html
- http://www.pro-football-reference.com/years/2016/probowl.htm
- http://www.sports-reference.com/cfb/ 
  - Combine & College football
- http://wonderlictestsample.com/nfl-wonderlic-scores/ 
  
## Usage / What I learned	:
This project focuses on gather messy data and transforming it into something that can be analyzed.

